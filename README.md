## raven-user Tiramisu TPB1.220310.029 8473704 release-keys
- Manufacturer: google
- Platform: gs101
- Codename: raven
- Brand: google
- Flavor: raven-user
- Release Version: 12
- Id: TPB1.220310.029
- Incremental: 8473704
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/raven/raven:Tiramisu/TPB1.220310.029/8473704:user/release-keys
- OTA version: 
- Branch: raven-user-Tiramisu-TPB1.220310.029-8473704-release-keys
- Repo: google_raven_dump_32701


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
